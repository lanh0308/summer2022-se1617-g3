/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.se1617.learning.controller.admin;

import com.se1617.learning.config.url.admin.UrlRegistrationsAdmin;
import com.se1617.learning.controller.base.BaseAuthController;
import com.se1617.learning.model.user.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author giaki
 */
public class RegistrationController extends BaseAuthController {

    @Override
    protected boolean isPermission(HttpServletRequest request) {
        User user = (User) request.getSession().getAttribute("user");
        return user.is_staff();
    }

    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_CREATE)) {
                registrationsCreateGet(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_EDIT)) {
                registrationsEditGet(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DELETE)) {
                registrationsDelete(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DETAIL)) {
                registrationsDetail(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_LIST)) {
                registrationsList(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DELETE_SELETED)) {
                registrationsDeleteSelected(request, response);
            } else {
                request.getRequestDispatcher("/views/error/admin/404.jsp").forward(request, response);
            }
        } catch (IOException | ServletException e) {
            request.getRequestDispatcher("/views/error/admin/500.jsp").forward(request, response);
        } catch (Exception ex) {
            request.setAttribute("code", ex.getMessage());
            request.getRequestDispatcher("/views/error/admin/access_denied.jsp").forward(request, response);
        }
    }

    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_CREATE)) {
                registrationsCreatePost(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_EDIT)) {
                registrationsEditPost(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DELETE)) {
                registrationsDelete(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DETAIL)) {
                registrationsDetail(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_LIST)) {
                registrationsList(request, response);
            } else if (super.getURI().matches(UrlRegistrationsAdmin.REGISTRATIONS_DELETE_SELETED)) {
                registrationsDeleteSelected(request, response);
            } else {
                request.getRequestDispatcher("/views/error/admin/404.jsp").forward(request, response);
            }
        } catch (IOException | ServletException e) {
            request.getRequestDispatcher("/views/error/admin/500.jsp").forward(request, response);
        } catch (Exception ex) {
            request.setAttribute("code", ex.getMessage());
            request.getRequestDispatcher("/views/error/admin/access_denied.jsp").forward(request, response);
        }
    }

    //-------------------- GET METHOD -------------------------//
    private void registrationsCreateGet(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsEditGet(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsDelete(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsDetail(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsList(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsDeleteSelected(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    //---------------------------POST METHOD -----------------------------//
    private void registrationsCreatePost(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private void registrationsEditPost(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
